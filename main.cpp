#include <iostream>
#include <string>
using namespace std;

struct Knoten{
    int wert;
    Knoten* nextL; // Knoten unten links angehängt
    Knoten* nextR; // Knoten unten rechts angehängt
    string ort; // Beispiel: "LLRRL"
};

Knoten* wurzel;
int anzahl = 0;
int maxTiefe = 0;

bool hatNachfolger(string KnotenWo, string seite){
        Knoten* knoten;
        knoten = wurzel;
        
        // Finde Knoten, an den angehängt werden soll:
        for(int k=0; k<KnotenWo.size(); k++){
            if(KnotenWo[k]=='L') {
                knoten = knoten->nextL;
            }
            if(KnotenWo[k]=='R') {
                knoten = knoten->nextR;
            }
        }
        if(seite == "L" && knoten->nextL==nullptr)
            return false;
        else if(seite == "R" && knoten->nextR==nullptr)
            return false;
        else
            return true;
        
}

string gibWert(string ortK){
    Knoten* knoten;
    knoten = wurzel;
    string ergebnis;
    // Wenn nicht vorhanden, dann --, sonst Zahl.
    for(int k=0; k<ortK.size(); k++){
        if(ortK[k]=='L' && knoten->nextL!=nullptr) 
            knoten = knoten->nextL;
        else if(ortK[k]=='R' && knoten->nextR!=nullptr) 
            knoten = knoten->nextR;
        else return "--";
    }
    if(ortK=="") return to_string(wurzel->wert);

    return to_string(knoten->wert);
}

void WurzelFestlegen(int wertWurzel){
    wurzel = new Knoten();
    wurzel->ort="";
    wurzel->wert=wertWurzel;
    wurzel->nextL=nullptr;
    wurzel->nextR=nullptr;
    maxTiefe=0; // Wurzel Tiefe ist Null.
}

void Befestigen(string KnotenWo, string seite, int wertK){
        Knoten* knoten;
        knoten = wurzel;
        
        // Finde Knoten, an den angehängt werden soll:
        for(int k=0; k<KnotenWo.size(); k++){
            if(KnotenWo[k]=='L') {
                knoten = knoten->nextL;
            }
            if(KnotenWo[k]=='R') {
                knoten = knoten->nextR;
            }
        }
        
        // Befestige daran den neuen Knoten:
        Knoten* neuKnoten;
        neuKnoten = new Knoten();
        neuKnoten->wert = wertK;
        
        if(seite=="L") {
            knoten->nextL = neuKnoten;
            neuKnoten->ort = KnotenWo + "L";
        }
        if(seite=="R") {
            knoten->nextR = neuKnoten;
            neuKnoten->ort = KnotenWo + "R";
        }
        if(neuKnoten->ort.size() > maxTiefe)
            maxTiefe++;
        // cout << endl << maxTiefe << endl;
}

void BaumKomplettFuellen(){
    wurzel->wert = 10;
    Befestigen("", "L", 11);
    Befestigen("", "R", 12);
    
    Befestigen("L", "L", 13);
    Befestigen("L", "R", 14);
    Befestigen("R", "L", 15);
    Befestigen("R", "R", 16);
    
    Befestigen("LL", "L", 17);
    Befestigen("LL", "R", 18);
    Befestigen("LR", "L", 19);
    Befestigen("LR", "R", 20);
    
    Befestigen("RL", "L", 21);
    Befestigen("RL", "R", 22);
    Befestigen("RR", "L", 23);
    Befestigen("RR", "R", 24);
    
    Befestigen("LLL", "L", 25);
    Befestigen("LLL", "R", 26);
    Befestigen("LLR", "L", 27);
    Befestigen("LLR", "R", 28);
    
    Befestigen("LRL", "L", 28);
    Befestigen("LRL", "R", 29);
    Befestigen("LRR", "L", 30);
    Befestigen("LRR", "R", 31);
    
    Befestigen("RLL", "L", 32);
    Befestigen("RLL", "R", 33);
    Befestigen("RLR", "L", 34);
    Befestigen("RLR", "R", 35);
    
    Befestigen("RRL", "L", 36);
    Befestigen("RRL", "R", 37);
    Befestigen("RRR", "L", 38);
    Befestigen("RRR", "R", 39);
}

string gibPfad(int n, int anzahl){
    //Liefert Pfad (z.B. LLR) durch Umwandlung von Dez in Binär.
    string p; // Pfad p
    while(n!=0) {
        // Rest von n/2 ist 0? Dann 0(=L), sonst die 1(=R).
        p=(n % 2 == 0 ? "L" : "R" ) + p; 
        n = n/2;
    }
    if(p.size()<anzahl)
        while(p.size()<anzahl)
            p = "L" + p;
    return p;
}

void ZeigeBaumNeu(){
    int RandAbstand = 0;
    int NachbarAbstand = 0;
    int AnzahlInZeile = 1; // Anzahl Einträge in Zeile
    
    // Anfangs-Randabstand ermitteln:
    for(int k=0;k<maxTiefe;k++)
        RandAbstand = RandAbstand * 2 + 2;   
    
    // a ist die Anzahl der Buchstaben eines Pfades, z.B.: a=3 bei LRL.
    for(int a=0;a<maxTiefe+1;a++){ // Schleife für Zeile
        
        
        cout << ".";
    
        for(int k=0;k<RandAbstand;k++) // Vorderster Eintrag
           cout << " ";
        if(gibWert(gibPfad(0, a))!="--") 
           cout << gibWert(gibPfad(0, a));
        else
           cout << "  ";

        NachbarAbstand = RandAbstand * 2 + 2;
    
        for(int j=1;j<=AnzahlInZeile-1;j++) {// ab 2. Eintrag
            for(int k=1;k<=NachbarAbstand;k++)
                cout << " ";
            if(gibWert(gibPfad(j, a))!="--") 
                cout << gibWert(gibPfad(j, a));
            else
                cout << "  ";
        }
        cout << endl;
        

        if(a!=maxTiefe){
            cout << ".";
            // Zwei Schleifen für die Zweige / und \:
            for(int k=0;k<RandAbstand-1;k++)// Vorderster Eintrag
                cout << " ";
            if(gibWert(gibPfad(0, a))!="--") {
                if(hatNachfolger(gibPfad(0, a),"L")==true)
                    cout << "/"; 
                else
                    cout << " ";
                if(hatNachfolger(gibPfad(0, a),"R")==true)
                    cout << "  \\"; 
                else
                    cout << "   ";
            }
                    //cout << "/  \\";
                
            else
                cout << "    ";
    
            NachbarAbstand = RandAbstand * 2 + 2;
     
            for(int j=1;j<=AnzahlInZeile-1;j++) {// ab 2. Eintrag
                for(int k=1;k<=NachbarAbstand-2;k++)
                    cout << " ";
                if(gibWert(gibPfad(j, a))!="--"){
                    if(hatNachfolger(gibPfad(j, a),"L")==true)
                        cout << "/"; 
                    else
                        cout << " ";
                    if(hatNachfolger(gibPfad(j, a),"R")==true)
                        cout << "  \\"; 
                    else
                        cout << "   ";
                }
                else
                    cout << "    ";
            }
            cout << endl;
        }
    
        AnzahlInZeile *= 2;
        RandAbstand = (RandAbstand - 2) / 2;
    }
    
    
    cout << endl;
}

int main() {
    WurzelFestlegen(60);
    Befestigen("", "L", 15);
    Befestigen("L", "R", 20);
    Befestigen("L", "L", 25);
    Befestigen("LL", "L", 30);
    Befestigen("", "R", 45);
    Befestigen("R", "R", 78);
    Befestigen("LR", "L", 22);
    Befestigen("LR", "R", 25);
    
    ZeigeBaumNeu();
    
    string pfad;
    string seite;
    int wert;
    string beenden = "N";
    
    while (beenden != "J"  && beenden != "j") {
        cout << "Wo wollen Sie etwas anhaengen? L=Links, R=Rechts" << endl;
        cout << "Geben Sie einen Pfad ein (zum Beispiel LLR)" << endl << "und drücken Sie dann Enter!" << endl;
        cin >> pfad;
        for(int k=0;k<pfad.size();k++) { // Buchstaben ggf. umwandeln
            if(pfad[k]=='l') pfad[k] = 'L';
            if(pfad[k]=='r') pfad[k] = 'R';
        }
        seite = pfad[pfad.size()-1];  // Letzter Buchstabe entspricht Seite
        pfad.pop_back(); // letzten Eintrag vom Pfad löschen
        cout << "Welchen Wert eintragen? Geben Sie eine" << endl;
        cout << "ganze Zahl zwischen 10 und 99 ein (z.B. 80)"<< endl;
        cout << "und drücken Sie dann Enter!" << endl;
        cin >> wert;
        
        Befestigen(pfad, seite, wert);

        // Clear Console-Screen:
        cout << "\x1B[2J\x1B[H";

        ZeigeBaumNeu();
        cout << "Programm beenden? (J/N): ";
        cin >> beenden;
    }
    cout << endl << "-- Programm-Ende --";
    return 0;
}